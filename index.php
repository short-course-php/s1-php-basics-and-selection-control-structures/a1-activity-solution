<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1 - Basic PHP</title>
</head>
<body>

	<div style="display: flex; justify-content: space-evenly;">
		<div>
			<h1>Problem #1</h1>
			<h3>Full Address: <?php echo getFullAddress('Philippines', 'Cebu', 'Basak', '44b Velez Street')?></h3>
		</div>

		<div>
			<h1>Problem #2</h1>
			<h3>Grade (99): <?php echo getLetterGrade(99); ?></h3>
			<h3>Grade (91): <?php echo getLetterGrade(91); ?></h3>
			<h3>Grade (85): <?php echo getLetterGrade(85); ?></h3>
			<h3>Grade (82): <?php echo getLetterGrade(82); ?></h3>
			<h3>Grade (72): <?php echo getLetterGrade(72); ?></h3>
			<h3>Grade (105): <?php echo getLetterGrade(105); ?></h3>			
		</div>
	</div>

</body>
</html>