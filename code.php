<?php 
	//Activity
	

	//1. Get Full Address
	function getFullAddress($country, $city, $province, $specific){
		return "$specific, $province, $city, $country";
	}

	//2. Letter Grade
	function getLetterGrade($grades){
		if($grades >= 98 && $grades <= 100){
			return "A+";
		}else if($grades >= 95 && $grades <= 97){
			return "A";
		}else if($grades >= 92 && $grades <= 94){
			return "A-";
		}else if($grades >= 89 && $grades <= 91){
			return "B+";
		}else if($grades >= 86 && $grades <= 88){
			return "B";
		}else if($grades >= 83 && $grades <= 85){
			return "B-";
		}else if($grades >= 80 && $grades <= 82){
			return "C+";
		}else if($grades >= 77 && $grades <= 79){
			return "C";
		}else if($grades >= 75 && $grades <= 76){
			return "C-";
		}else if($grades <= 74){
			return "D";
		}else{
			return "You should enroll to Harvard!";
		}
	}



?>